import math

try:
    ugol_v_gradusax = int(input('Введите величину угла (в градусах): '))
except ValueError:
    print('Некорректный ввод')
    exit()
ugol_v_radianax = math.radians(ugol_v_gradusax)
print(f'sin({ugol_v_gradusax})={math.sin(ugol_v_radianax)}')
print(f'cos({ugol_v_gradusax})={math.cos(ugol_v_radianax)}')
print(f'tg({ugol_v_gradusax})={math.tan(ugol_v_radianax)}')

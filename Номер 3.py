import random

count = random.randrange(4, 30 + 1)
print(f'В куче {count} камней\n')
while count != 1:
    if count == 4:
        print(f'Ход противника: 3')
        print(f'В куче осталось 1 камней')
        print('Выиграл противник')
        exit()

    if count == 3:
        print(f'Ход противника: 2')
        print(f'В куче осталось 1 камней')
        print('Выиграл противник')
        exit()

    if count == 2:
        print(f'Ход противника: 1')
        print(f'В куче осталось 1 камней')
        print('Выиграл противник')
        exit()

    hod_pr = random.randrange(1, 3 + 1)
    print(f'Ход противника: {hod_pr}')
    count -= hod_pr
    print(f'В куче осталось {count} камней')

    while True:
        try:
            hod_tv = int(input('Выберите, сколько камней вы возьмёте из кучи (1, 2 или 3): '))
            if hod_tv < 1 or hod_tv > 3:
                print('Некорректный ввод, попробуйте ещё раз')
            if 1 <= hod_tv <= 3:
                if count - hod_tv >= 1:
                    break
                else:
                    print('Некорректный ввод, попробуйте ещё раз')
        except ValueError:
            print('Некорректный ввод, попробуйте ещё раз')

    count -= hod_tv
    print(f'В куче осталось {count} камней\n')
else:
    print('Ты выиграл!!!')
